import {AppRegistry} from 'react-native';
import App from './app/App';
import './app/assets/globals';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
