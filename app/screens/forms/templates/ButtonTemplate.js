import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

function buttonTemplate(locals) {
  if (locals.hidden) {
    return null;
  }

  let stylesheet = locals.stylesheet;

  return (
    <View style={stylesheet.formGroup.normal}>
      <TouchableOpacity
          activeOpacity={0.75}
          style={stylesheet.button}
          onPress={(event) => {locals.onChangeNative(event);}}>
        <Text style={stylesheet.buttonText}>{locals.label}</Text>
      </TouchableOpacity>
    </View>
  );
}

module.exports = buttonTemplate;
