import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { colors } from '../../../assets/styles/base';
import { AllHtmlEntities } from 'html-entities';
import HTMLView from 'react-native-htmlview';

function overviewTemplate(locals) {
  if (locals.hidden) {
    return null;
  }

  let stylesheet = locals.stylesheet;

  entities = new AllHtmlEntities();

  return (
    <View style={stylesheet.formGroup.normal, styles.paragraph}>
      <HTMLView value={entities.decode(locals.value)} style={{flex:1, flexDirection: 'row', alignItems: 'flex-start', flexWrap: 'wrap'}}  stylesheet={styles}/>
    </View>
  );
}

module.exports = overviewTemplate;

const styles = StyleSheet.create({
  paragraph:{
    marginHorizontal: 1,
  },
  b:{
    fontWeight:'bold',
  },
})
