import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colors } from '../../../assets/styles/base'

function overviewTemplate(locals) {
  if (locals.hidden) {
    return null;
  }

  let stylesheet = locals.stylesheet;

  return (
    <View style={stylesheet.formGroup.normal, styles.overview}>
      {
        locals.value.map(step =>
          <View key={step.stepTitle}>
            <Text style={stylesheet.controlLabel.normal, styles.stepTitle}>{step.stepTitle}</Text>
            {
              step.fields.map(field =>
                <View key={field.name} style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }}>
                  <Text style={{ flex: 1, alignSelf: 'stretch' }}> {field.name} </Text>
                  <Text style={{ flex: 1, alignSelf: 'stretch' }}> {field.description != 'null' ? field.description : ''} </Text>
                </View>
              )
            }
          </View>
        )
      }
    </View>
  );
}

module.exports = overviewTemplate;

const styles = StyleSheet.create({
  overview:{
    borderWidth: 0.5,
    borderColor: colors.gx_dark_blue,
    backgroundColor: colors.background_disabled,
    marginHorizontal: 5,
  },
  stepTitle:{
    fontWeight: "bold",
  }
})
