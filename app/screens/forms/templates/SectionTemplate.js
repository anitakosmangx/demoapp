import React from 'react';
import { View, Text } from 'react-native';
import ShowHideView from '../components/ShowHideView';

function sectionTemplate(locals) {
  if (locals.hidden) {
    return null;
  }

  var stylesheet = locals.stylesheet;
  var controlLabelStyle = stylesheet.controlLabel.normal;

  if (locals.hasError) {
    controlLabelStyle = stylesheet.controlLabel.error;
  }

  var label = locals.label ? (
    <Text style={controlLabelStyle}>{locals.label.endsWith(" *") ? locals.label.substring(0, locals.label.length - 2) : locals.label}</Text>
  ) : <View />;
  var error =
    locals.hasError && locals.error ? (
      <Text accessibilityLiveRegion="polite" style={stylesheet.errorBlock}>
        {locals.error}
      </Text>
    ) : null;

  var rows = locals.order.map(function(name) {
    return locals.inputs[name];
  });

  return (
    <ShowHideView
      show={false}
      label={label}
    >
      <View>
        {error}
        {rows}
      </View>
    </ShowHideView>
  );
}

module.exports = sectionTemplate;
