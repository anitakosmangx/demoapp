import React, { Component } from 'react';
import { Animated, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class ShowHideView extends Component {
  constructor(props){
    super(props);
    this.state = {
      show: props.show,
      animation: new Animated.Value(0),
      initialized: false
    };
  }

  render() {
    return (
      <Animated.View style={{ overflow:'hidden', height: this.state.initialized ? this.state.animation : 'auto' }}>
        <TouchableOpacity
          onPress={this.toggle}
          onLayout={this.setMinHeight.bind(this)}
          style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}
        >
          <Text>{this.props.label}</Text>
          <Icon style={{margin:10}} name={this.state.show? 'chevron-up' : 'chevron-down'}/>
        </TouchableOpacity>
        <View onLayout={this.setMaxHeight.bind(this)} style={{margin:7}} >
          {this.props.children}
        </View>
      </Animated.View>
    );
  }

  toggle = () => {
    let finalValue = this.state.show? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

    this.setState({
        show : !this.state.show
    });

    Animated.spring(
      this.state.animation,
      { toValue: finalValue }
    ).start();
  }

  setMaxHeight(event){
    this.state.maxHeight = event.nativeEvent.layout.height;
    if(this.state.minHeight && !this.state.initialized)
      this.onCalculated("max");
  }

  setMinHeight(event){
    this.state.minHeight = event.nativeEvent.layout.height;
    if(this.state.maxHeight && !this.state.initialized)
      this.onCalculated("min");
  }

  onCalculated(calculated){
    let initialValue = this.state.maxHeight + this.state.minHeight;
    let finalValue = this.state.show? this.state.maxHeight + this.state.minHeight : this.state.minHeight;

    this.setState({
        initialized : true
    });

    this.state.animation.setValue(initialValue);
    Animated.spring(
      this.state.animation,
      { toValue: finalValue }
    ).start();
  }
}