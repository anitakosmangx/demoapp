import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import t from 'tcomb-form-native';

class Upload extends t.form.Textbox {
  constructor(props) {
    super(props);
  }

  getLocals() {
    let locals = super.getLocals();
    locals.acceptedTypes = this.props.options.acceptedTypes;
    return locals;
  }

  getTemplate() { return(locals => {
    if (locals.hidden) {
      return null;
    }

    let stylesheet = locals.stylesheet;
    let controlLabelStyle = stylesheet.controlLabel.normal;
    let pickerTouchableStyle = stylesheet.pickerTouchable.normal;
    let helpBlockStyle = stylesheet.helpBlock.normal;
    let pickerValueStyle = stylesheet.pickerValue.normal;
    const errorBlockStyle = stylesheet.errorBlock;

    if (locals.hasError) {
      controlLabelStyle = stylesheet.controlLabel.error;
      pickerTouchableStyle = stylesheet.pickerTouchable.error;
      helpBlockStyle = stylesheet.helpBlock.error;
      pickerValueStyle = stylesheet.pickerValue.error;
    }

    const label = locals.label ? <Text style={controlLabelStyle}>{locals.label}</Text> : null;
    const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null;
    const error =
      locals.hasError && locals.error ? (
        <Text style={errorBlockStyle}>{locals.error}</Text>
      ) : null;
    let buttonText = 'Select a file';
    if (locals.value && locals.value != '') {
      try {
        buttonText = JSON.parse(locals.value).name;
      } catch(e) {
        locals.onChange(null);
      }
    }

    return (
      <View style={stylesheet.formGroup.normal}>
        {label}
        <TouchableOpacity
            activeOpacity={0.75}
            style={pickerTouchableStyle}
            onPress={event => this.pickDocument(locals)}>
            <Text style={pickerValueStyle}>{buttonText}</Text>
        </TouchableOpacity>
        {help}
        {error}
      </View>
    );
  });}

  pickDocument(locals) {
      try {
        DocumentPicker.pick(locals.acceptedTypes.length > 0 ? {type: locals.acceptedTypes} : null).then( pick =>
          locals.onChange(JSON.stringify(pick))
        )
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      }
    }
}

export default Upload;
