import React from 'react';
import { View, Text, Switch, StyleSheet } from 'react-native';
import t from 'tcomb-form-native';

class CheckboxList extends t.form.Select {
  constructor(props) {
    super(props);
    const locals = super.getLocals();
    const isChecked = {};
    const { options, value, onChange } = locals;
    this._onChange = this._onChange.bind(this);
    this.props.onChange = onChange;
    this.props.onChange(value);
 
    if(options){
      options.forEach(item => (isChecked[item.value] = value.includes(item.value)));
    }

    this.state = {
      isChecked,
    };
  }

  getTransformer() {
    return CheckboxList.transformer();
  }

  getTemplate() {
    return (locals) => {
      const stylesheet = locals.stylesheet;
      let formGroupStyle = stylesheet.formGroup.normal;
      let controlLabelStyle = stylesheet.controlLabel.normal;
      let checkboxStyle = stylesheet.checkbox.normal;
      let helpBlockStyle = stylesheet.helpBlock.normal;
      const errorBlockStyle = stylesheet.errorBlock;

      if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        controlLabelStyle = stylesheet.controlLabel.error;
        checkboxStyle = stylesheet.checkbox.error;
        helpBlockStyle = stylesheet.helpBlock.error;
      }

      const label = locals.label ? <Text style={controlLabelStyle}>{locals.label}</Text> : null;
      const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null;
      const error =
        locals.hasError && locals.error ? (
          <Text style={errorBlockStyle}>{locals.error}</Text>
        ) : null;

      const viewArr = [];

      locals.options.forEach((item, index) =>
        viewArr.push(<View key={index} style={{ flexDirection: "row" }}>
          <Text style={styles.option}>{item.name}</Text>
          <Switch
            style={checkboxStyle}
            key={index}
            ref={item.value}
            accessibilityLabel={item.name}
            value={this.state.isChecked[item.value]}
            onValueChange={(checked) => {
              this._onChange(item, locals, checked);
            }}
          /></View>, ), );

      return (
        <View style={formGroupStyle}>
          {label}
          {viewArr}
          {help}
          {error}
        </View>
      );
    };
  }

  getOptions() {
    const { options } = this.props;
    const items = options.options ? options.options.slice() : null;
    return items;
  }

  _onChange(item, locals, checked) {
    const { isChecked } = this.state;
    isChecked[item.value] = checked;
    const changeArr = [];

    Object.keys(isChecked).forEach(key => {
      isChecked[key] && changeArr.push(key);
    });

    this.props.onChange(changeArr);

    this.setState({
      isChecked,
    });
  }
}

CheckboxList.transformer = () => ({
  format: value => (Array.isArray(value) ? value : []),
  parse: value => value ? value : [] ,
});

const styles = StyleSheet.create({
  option:{
    fontSize: 15,
    paddingLeft: 8,
    flex: 1,
  }
})

export default CheckboxList;
