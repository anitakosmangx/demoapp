import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView, Platform } from 'react-native';
import t from 'tcomb-form-native';
import { fonts } from '../../assets/styles/base';
import buttonTemplate from "./templates/ButtonTemplate";
import overviewTemplate from "./templates/OverviewTemplate";
import paragraphTemplate from "./templates/ParagraphTemplate";
import Upload from "./factories/Upload";
import CheckboxList from "./factories/CheckboxList";
import radioTemplate from "./templates/RadioTemplate";
import sectionTemplate from "./templates/SectionTemplate";
import moment from "moment";
import mime from 'react-native-mime-types';
import getDateDayRangePredicate from "./validators/DateDayRangeValidator";
import getDateRangePredicate from "./validators/DateRangeValidator";
import getRegExpPredicate from "./validators/RegExpValidator";

const Form = t.form.Form;

export default class XCForm extends Component {
  constructor(props) {
    super(props)
    const form = this.findFormInContent(props.dataSource);
    this.state = this.initialize(form);
  }

  findFormInContent(content){
     return content.elements.find(element => element.type == 'form');
  }

  initialize(definition) {
    const state = {
      definition: definition,
    };

    if (definition && definition.errors) {
      state.errors = definition.errors;
    }

    if (definition && definition.fields) {
      let initialInputNames = 'formid,wmformid,wmstepid,wmlocale,clientsideRouting,button_pressed';
      let {structure, values, options, inputNames} = this.getStructureValuesOptionsAndInputNames(definition.fields, initialInputNames, false);
      state.structure = structure;
      state.values = values;
      state.options = options;

      // Calculate signature
      fetch(global.baseUrl + '/web/secureformssigner?inputNames=' + inputNames, {
        credentials: 'include',
      }).then(response => {
        if (response.status == 200) {
          response.text().then(text => {state.definition.swfrmsig = text});
        }
      });
    }

    return state;
  }

  getStructureValuesOptionsAndInputNames(fields, inputNames, isNested){
    let structure = {};
    let values= {};
    let options= isNested ? {
      fields: {}
    } : {
      auto: 'placeholders',
      i18n: {
        optional: '',
        required: ' *'
      },
      fields: {}
    };

    fields.forEach(field => {
      if(field.type == 'section'){
        let nested = this.getStructureValuesOptionsAndInputNames(field.value, inputNames, true);
        inputNames = nested.inputNames;
        structure[field.path] = t.struct(nested.structure);
        values[field.path] = nested.values;
        this.addFieldToOptions(field, options);
        options.fields[field.path].fields = nested.options.fields;
      } else {
        this.addFieldToStructure(field, structure);
        this.addFieldToValues(field, values);
        this.addFieldToOptions(field, options);
      }

      if (field.type == 'backbutton') {
        inputNames += ',wmback';
      } else if (field.type != 'button' && field.type != 'overview' && field.type != 'paragraph' && field.type != 'section') {
        inputNames += ',' + field.path;
      }
    });

    return {structure: structure, values: values, options: options, inputNames: inputNames};
  }

  addFieldToStructure(field, structure) {
    if (field.type == 'text' || field.type == 'password' || field.type == 'textarea' || field.type == 'email' || field.type == 'upload') {
      structure[field.path] = field.required ? t.String : t.maybe(t.String);
    } else if (field.type == 'date' || field.type == 'time') {
      structure[field.path] = field.required ? t.Date : t.maybe(t.Date);
    } else if (field.type == 'number') {
      structure[field.path] = field.required ? t.Number : t.maybe(t.Number);
    } else if (field.type == 'radio' || field.type == 'dropdown') {
      structure[field.path] = field.required ? t.enums(field.items) : t.maybe(t.enums(field.items));
    } else if (field.type == 'checkbox') {
      structure[field.path] = t.list(t.String);
    } else if (field.type == 'overview') {
      structure[field.path] = t.maybe(t.Any);
    } else if (field.type == 'backbutton' || field.type == 'button' || field.type == 'paragraph') {
      structure[field.path] = t.maybe(t.String);
    }

    this.addFieldValidators(field, structure);
  }

  addFieldValidators(field, structure) {
    if (field.validations) {
      field.validations.forEach( validator => {
        if (validator.id == 'DateDayRangeValidator') {
          structure[field.path] = t.refinement(structure[field.path], getDateDayRangePredicate(validator));
        } else if (validator.id == 'DateRangeValidator') {
          structure[field.path] = t.refinement(structure[field.path], getDateRangePredicate(validator));
        } else if (validator.id == 'RegExpValidator') {
          structure[field.path] = t.refinement(structure[field.path], getRegExpPredicate(validator));
        }
      })
    }
  }

  addFieldToValues(field, values) {
    if (field.value && field.value != '') {
      if (field.type == 'date') {
        values[field.path] = moment(field.value, "DD-MM-YYYY").toDate();
      } else if (field.type == 'time') {
        values[field.path] = moment(field.value, "HH:mm").toDate();
      } else {
        values[field.path] = field.value;
      }
    }
  }

  addFieldToOptions(field, options) {
    options.fields[field.path] = {
      label: field.label,
      placeholder: field.extraText && field.extraText != '' ? field.extraText : ' ',
      help: field.helpText ? field.helpText : '',
      hidden: !field.visible,
      hasError: field.hasError
    };

    if (field.errors) {
      let errorMessage = '';
      field.errors.forEach(error => {
        errorMessage += (errorMessage.length > 0 ? '\n' : '') + error.message;
      })
      options.fields[field.path].error = errorMessage;
    }

    if (field.type == 'password') {
      options.fields[field.path].secureTextEntry = true;
    } else if (field.type == 'email') {
      options.fields[field.path].keyboardType = 'email-address';
    } else if (field.type == 'textarea') {
      options.fields[field.path].multiline = true;
      options.fields[field.path].stylesheet = textarea;
    } else if (field.type == 'date') {
      options.fields[field.path].mode = 'date';
      options.fields[field.path].config = {
        format : (date) => moment(date).format("DD-MM-YYYY"),
        defaultValueText: "Tap to select a date"
       };
    } else if (field.type == 'time') {
      options.fields[field.path].mode = 'time';
      options.fields[field.path].config = {
        format : (date) => moment(date).format("HH:mm"),
        defaultValueText: "Tap to select a time"
       };
    } else if (field.type == 'button' || field.type == 'backbutton') {
      options.fields[field.path].template = buttonTemplate;
      options.fields[field.path].onChange = (() => this.onSubmit(field.type == 'backbutton' ? 'wmback' : field.id));
    } else if (field.type == 'overview') {
      options.fields[field.path].template = overviewTemplate;
      options.fields[field.path].editable = false;
    } else if (field.type == 'paragraph') {
      options.fields[field.path].template = paragraphTemplate;
      options.fields[field.path].editable = false;
    } else if (field.type == 'checkbox') {
      options.fields[field.path].options = field.items;
      options.fields[field.path].factory = CheckboxList;
    } else if (field.type == 'radio') {
      options.fields[field.path].template = radioTemplate;
    } else if (field.type == 'upload') {
      options.fields[field.path].factory = Upload;
      options.fields[field.path].acceptedTypes = this.getAcceptedTypes(field.acceptedExtensions, field.acceptedMimeTypes);
    } else if (field.type == 'section') {
      options.fields[field.path].template = sectionTemplate;
    }
  }

  getAcceptedTypes(acceptedExtensions, acceptedMimeTypes) {
    if (Platform.OS == 'ios'){
       // UTI's not supported yet
      return null;
    } else {
      if (acceptedMimeTypes == null){
        acceptedMimeTypes = [];
      }
      acceptedExtensions.forEach(extension =>
        acceptedMimeTypes.push(mime.contentType(extension))
      );
      return acceptedMimeTypes == [] ? null : acceptedMimeTypes;
    }
  }

  onSubmit(button) {
    let values = this.refs.form.getValue();
    if(values) {
      fetch(this.state.definition.actionUrl, {
        method: 'POST',
        credentials: 'include',
        body: this.constructRequestBody(values, button)
      }).then(response => {
        if (response.ok) {
          response.json().then(json => {
            if (json.routingResult.step) {
              // Routing to step
              fetch(this.state.definition.ajaxUrl + '&wmstepid=' + json.routingResult.step, {
                credentials: 'include',
              }).then(nextStepResponse => {
                if (nextStepResponse.status == 200) {
                  nextStepResponse.json().then(nextStepJson => {
                    const form = this.findFormInContent(nextStepJson.contents[0]);
                    this.setState(this.initialize(form));
                  })
                }
              }).catch (error => {console.error(error)});
            } else {
              // Routing to page
              if (this.props.onFinish) {
                this.props.onFinish();
              }
            }
          });
        } else {
          console.warn(response);
        }
      }).catch (error => {console.error(error)});
    } else {
       console.warn('validation failed');
    }
  }

  constructRequestBody(values, button) {
    let formData = new FormData();
    ['formid', 'wmformid', 'wmstepid', 'wmlocale', 'swfrmsig'].forEach(name => formData.append(name,this.state.definition[name]));
    formData.append('clientsideRouting', 'true');

    this.state.definition.fields.forEach(field => {
      if (field.type == 'section') {
        field.value.forEach(child => {
          this.addFieldToFormData(formData, values[field.path], child);
        });
      } else if (field.type != 'button' && field.type != 'backbutton' && field.type != 'overview' && field.type != 'paragraph') {
        this.addFieldToFormData(formData,values,field);
      }
    })

    if (button == 'wmback') {
      formData.append('wmback','true');
    } else {
      formData.append('button_pressed', button);
    }

    return formData;
  }

  addFieldToFormData(formData, values, field){
    let value = values == null ? null : values[field.path];
    if (field.type == 'date'){
      formData.append(field.path, value == null ? "" : moment(value).format("DD-MM-YYYY"));
    } else if (field.type == 'time'){
      formData.append(field.path, value == null ? "" : moment(value).format("HH:mm"));
    } else if (field.type == 'upload'){
      if(value != null) {
        value = JSON.parse(value);
        formData.append(field.path, {uri: value.uri, type:value.type, name:value.name});
      }
    } else if(field.type == 'checkbox') {
      if(value != null && value.size > 0){
        value.forEach(item =>
          formData.append(field.path, item)
        );
      } else {
        formData.append(field.path, '');
      }
    } else {
      formData.append(field.path, value == null ? "" : value);
    }
  }

  render() {
    if (this.props.dataSource) {
      const errorBlockStyle = Form.stylesheet.errorBlock;
      const errors = this.state.errors ? this.state.errors.map(error =>
        <Text key="errors" style={errorBlockStyle}>{error.message}</Text>
      ) : null;
      return (
        <ScrollView>
          <Text style={styles.stepTitle}>{this.props.dataSource.step}</Text>
          <Form ref="form"
            type={t.struct(this.state.structure)}
            value={this.state.values}
            options={this.state.options}
          />
          {errors}
        </ScrollView>
      )
    } else {
      return <View />
    }
  }
}

const styles = StyleSheet.create({
  stepTitle: {
    fontSize: 24,
    fontFamily: fonts.semibold
  }
});

const textarea = {
    ...Form.stylesheet,
    textbox: {
        ...Form.stylesheet.textbox,
        normal: {
            ...Form.stylesheet.textbox.normal,
            height: 90,
            textAlignVertical: 'top',
        },
        error: {
            ...Form.stylesheet.textbox.error,
            height: 90,
        }
    }
};
  