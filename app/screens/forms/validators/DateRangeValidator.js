function getDateRangePredicate(validator) {
  let fromDate = validator.fromdate;
  let toDate = validator.todate;
  return function(d) {
    if (!d || fromDate == '' && toDate == '')
      return true;

    fromDate = fromDate.replace(/-/gi, '/');
    var fromSplitted = fromDate.split('/');
    var fromD = new Date(fromSplitted[1] + "/" + fromSplitted[0] + "/" + fromSplitted[2]);

    toDate = toDate.replace(/-/gi, '/');
    var toSplitted = toDate.split('/');
    var toD = new Date(toSplitted[1] + "/" + toSplitted[0] + "/" + toSplitted[2]);

    return fromD < d && d < toD;
  };
}

module.exports = getDateRangePredicate;