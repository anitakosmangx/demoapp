function getDateDayRangePredicate(validator) {
  const nrOfDays = parseInt(validator.nr_days);
  return function(d) {
    if (!d)
      return true;
    if (nrOfDays != 'NaN') {
      var currentDate = new Date();
      var checkDate = new Date();
      checkDate.setDate(checkDate.getDate() + nrOfDays);

      if (nrOfDays >= 0) {
        return d >= currentDate && d <= checkDate;
      } else {
        return d <= currentDate && d >= checkDate;
      }
    } else {
      return false;
    }
  };
}

module.exports = getDateDayRangePredicate;