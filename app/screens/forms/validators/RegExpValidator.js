function getRegExpPredicate(validator) {
  const regexp = new RegExp(validator.regexp);
  return function(s) {
    if (!s || s == "")
      return true;

    return regexp.test(s);
  };
}

module.exports = getRegExpPredicate;