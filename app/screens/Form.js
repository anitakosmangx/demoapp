import React, { Component } from 'react';
import { Text, View } from 'react-native';
import XCForm from './forms/XCForm'

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataSource: [],
    }
  }

  componentDidMount() {
    this.fetchPage();
  }

  async fetchPage() {
    this.state = {
      loading: true,
      dataSource: [],
    }

    try {
      const response = await fetch(global.baseUrl + ('/web/react-native-form.htm'), {
        credentials: 'include'
      });
      if (!response.ok) {
        throw Error(response.statusText);
      }
      const json = await response.json();
      this.setState({
        dataSource: json.contents,
        loading: false,
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    if (!this.loading && this.state.dataSource[0] && this.state.dataSource[0].elements && this.state.dataSource[0]) {
      return (
          <XCForm dataSource={this.state.dataSource[0]} />
      )
    }
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Loading</Text>
      </View>
    );
  }
}
