import React from 'react';
import { StyleSheet } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

import Home from './screens/Home';
import Form from './screens/Form'

const TabNavigation = createBottomTabNavigator({
    Home: { screen: Home,
            navigationOptions: () => ({
                tabBarIcon: ({focused, tintColor}) => (
                  <FontAwesome style={[styles.icon, focused ? styles.focused : styles.default]}>{Icons.home}</FontAwesome>
                )
            })
          },
    Form: { screen: Form,
            navigationOptions: () => ({
                tabBarIcon: ({focused, tintColor}) => (
                  <Icon name="iaf" style={[styles.icon, focused ? styles.focused : styles.default]} />
                )
            })
          }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarOnPress: () => {
        if (navigation.isFocused() && navigation.state.index > 0) {
          // Pop to root on tab bar pressed if already focused on the tab
          navigation.popToTop();
        } else {
          // Scroll to top if at root of tab
          const navigationInRoute = navigation.getChildNavigation(
            navigation.state.routes[0].key
          );

          if (
            !!navigationInRoute &&
            navigationInRoute.isFocused() &&
            !!navigationInRoute.state.params &&
            !!navigationInRoute.state.params.scrollToTop
          ) {
            navigationInRoute.state.params.scrollToTop();
          } else {
            navigation.navigate(navigation.state.key);
          }
        }
      }
    }),
    tabBarOptions: {
      activeTintColor: '#333a56',
      inactiveTintColor: '#52658f',
      activeBackgroundColor: '#f9f9f9',
      inactiveBackgroundColor: '#f9f9f9',
      showLabel: false,
      style: {
        backgroundColor: '#f9f9f9',
      }
    },
  }
);

const styles = StyleSheet.create({
  icon: {
      fontSize: 24,
  },
  icon_s: {
      fontSize: 22,
  },
  icon_xs: {
      fontSize: 20,
  },
  focused: {
      color: '#52658f',
  },
  default: {
      color: '#333',
  }
});

const AppContainer = createAppContainer(TabNavigation);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }

  componentDidMount() {
  Icon.getImageSource('iaf', 30).then(source =>
      this.setState({ iafIcon: source })
    );}
}
