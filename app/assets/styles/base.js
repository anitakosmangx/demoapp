import { Dimensions } from 'react-native'

export const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}

export const colors = {
  /** branding */
  gx_dark_blue: '#52658f',
  gx_light_blue: '#333a56',

  /* font colors */
  font_default: '#232323',
  font_grey: '#939393',
  font_light: '#c0c0c0',

  /* background colors */
  background_dark: '#191919',
  background_disabled: '#eeeeee',
  background_light: '#f9f9f9',
}

export const padding = {
  sm: 10,
  md: 20,
  lg: 30,
  xl: 40
}

export const fonts = {
  sm: 12,
  md: 18,
  lg: 28,
  regular: 'Fira Sans',
  italic: 'Fira Sans Italic',
  medium: 'Fira Sans Medium',
  semibold: 'Fira Sans SemiBold',
}
